# A Year at a Glance: Hosco Tech in 2019

We’re always very focused on our day-to-day tasks, but from time to time it’s good to take a step back and remind ourselves what we’ve achieved. The end of an old year and the beginning of a new one seems like the perfect opportunity to do just that.

2019 in numbers:

- We created our 3rd Tech&Product squad, in charge of B2B retention
- We deployed 16 new microservices (we now have 28 microservices in production)
- We deployed more than 900 times to production (monolith and microservices)
- We released more than 10 new versions in both Android and iOS

Throughout the year, our key focus was on automation and the improved scalability of our services, as well as making deployments more accessible and faster for everyone. Here’s a closer look at what we did:

## Improved stability of the platform

During the first semester, we started to see some scalability issues on our monolith.
Every day, at the time we were sending job alert emails to our users, the website and the mobile applications were becoming very slow.

We decided to decouple some parts into smaller microservices and databases. This helped us to halve our average response on the monolith (~ 250ms), and made sure that the sending of daily emails to our members wouldn’t affect other services.

## From Rancher to EKS

In 2018 we migrated all our microservices and our monolith to [Rancher](https://rancher.com/).

Rancher is great to get started with K8s, as the UI is clear and simple, but we started to face some limitations.
After a few months, we saw that our team was more comfortable with Kubernetes, so we decided to remove the Rancher layer to reduce the overhead.

We decided to go for EKS, as it is managed by AWS and the price/service is totally worth it.
We moved our development environments to AWS EKS in June, and we moved our production cluster in August (it took us only 3 days to migrate production).

Now all our services run on Kubernetes, even Postgres and ElasticSearch in development environments. Our developers are more familiar with Kubernetes, it's really easy to deploy and monitor new microservices.

## Helm

In order to speed up the deployment of new microservices, we decided to use [Helm](https://helm.sh/). We created very configurable charts so our microservices only need to define very few values.

Using Helm also enabled us to simplify/automate our dev environments. QA and developers can deploy features to disposable environments, using their own databases.

Helm allowed us to simplify our deployments and make our microservice configuration easier to read and write.

## Minikube

After moving to EKS, it was time to replace our ‘good old’ docker-compose setup in use locally.

Docker-compose works well, but we needed something closer to production. We were also facing some limitations as it’s more complicated to integrate different microservices with each other and we cannot reuse our ingress rules, etc.

With Minikube we can manage our microservices as we do in development or production. Checking the logs, metrics, and deployments is very easy. We explain in more detail how and why we did this migration in [this article](https://tech.hosco.com/minikube-a-safe-environment/).

## Code reviews & automation

Code reviews are very important and are mandatory at Hosco. No one can deploy without getting two approvals from their peers. It's a key moment where we can spot all kinds of issues including bugs, edge cases and code smells... but it also brings a risk of getting caught up on details, losing the point and delaying the delivery of the feature.

Code style is a matter of taste. Instead of having a sterile debate about code style in our code reviews, we preferred to use code formatters. Our code is automatically formatted by our code editor (all the code editors have plugins for this), so we are sure that the code we push is properly formatted.

Developers and reviewers can now focus on the logic, which speeds up the development and the deployment of new features.

We use:

- GoLang: go fmt
- Javascript / Typescript / HTML: prettier
- PHP: php-cs-fixer

## CI/CD Improvements

We improved our Gitlab pipelines by autoscaling our runners. Before, we had a fixed number of runners, regardless of their usage, and we started to have issues when too many pipelines were running at the same time.

We also needed to stop these instances outside of working hours to save cost. This is not needed anymore, runners are started and stopped automatically depending on the usage.

Developers are happy, and Finance too :)

This year we also setup the CI pipelines for iOS on Bitrise (tests + deployments). Now all our applications and microservices have their CI/CD pipelines integrated.

## Back-office VueJS

We also started to develop our new back-office with VueJS. VueJS is a very nice and simple front-end framework. It started to get very popular, so we thought it was a good idea to keep an eye on it and start playing with it.

This back-office will help us to improve our automation and save our developers time on a lot of small operational requests.

## React & SSR

Most of our web pages are still served by our old monolith, an ugly mix of PHP and AngularJS. In the second half of 2019, we decided to rebuild our job directory pages with React.

We wanted to do SSR (Server Side Rendering) in order to improve SEO (improve crawl budget), so we used [NextJS](https://nextjs.org/).

We’re continuing the migration into 2020, with the aim that most of our pages will be served by the NextJS application, and our monolith will only contain the legacy API (strangler pattern).

Stay tuned, we'll be posting about this migration soon ;)

## Design system and style guide

With the new front-end migration, we also decided to create our Design System and to embrace the Atomic design principles.

This will help us to speed-up our frontend-developments, make sure our UI is consistent, modular and easy to maintain. It also helps us to improve the communication between designers and developers.

In our monolith we already used Sass, so we already had variables and mixins which made it easier to define our particles and atoms.

With React it is also very easy to create atomic and reusable components. We also follow the presentational/container principles, so we can document/test our presentational components easily in our [styleguide](https://styleguide.hosco.com).

## Mobile apps: modularization and new versions

Our mobile team began a modularization project, splitting the code into smaller modules to make our applications easier to maintain and faster to build. The idea is also to build reusable modules between iOS and Android in Kotlin, to speed up development and increase code reusability.

On top of that, we rebuilt the whole navigation on Android with a new dashboard for job seekers. On iOS we released a new design for member profiles and improved the experience when editing your profile.

All these releases helped us to consolidate our user's happiness. We have now a rating of 4.4 on Android and 4.6 on iOS.

## What's next?

In 2019 we’ve been focused on automation and decoupling. We continued to strangle our poor monolith, added some nice technologies to our stack, that will help us to be more performant.

Our objective is to never stop learning, in order to continuously improve our products and our user happiness.

In 2020, we'd like to:

- investigate service mesh
- migrate the missing development databases/services to EKS
- use terraform and ansible to manage our infrastructure
- add more features to our back office / more automation
- continue to decouple our monolith
- continue the React migration
- continue the modularization of our mobile apps
- set up instrumentation tests for our multi-module architecture on mobile
- experiment with Swift UI
- continue hiring (we plan to hire a complete new squad)

Thanks for reading!

We are looking for new talents, [apply now](https://www.linkedin.com/jobs/view/1589535271/)
